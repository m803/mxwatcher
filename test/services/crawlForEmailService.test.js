const { expect } = require("@jest/globals");
const {
  crawlForEmailService,
  hasAlreadyCrawled,
} = require("../../src/services/crawlForEmailService");
import * as domainsClass from "../../src/services/generateDomains";
import * as repoClass from "../../src/database/emailServiceRepo";
import { count } from "yargs";

jest.mock("../../src/services/generateDomains");
jest.mock("../../src/database/emailServiceRepo");

beforeEach(() => {
  jest.mock("../../src/services/generateDomains");
  jest.mock("../../src/database/emailServiceRepo");
  domainsClass.generateDomains.mockResolvedValue(["a", "b"]);
});

afterEach(() => {
  jest.clearAllMocks();
});

test("should save to repo", async () => {
  const spy = jest.spyOn(repoClass, "saveMany").mockImplementation(() => {});
  await crawlForEmailService();

  expect(spy).toHaveBeenCalled();
});

test("should check already crawled", async () => {
  const spy = jest.spyOn(repoClass, "findAllBy").mockResolvedValue({
    count: () => 1000,
  });
  await hasAlreadyCrawled();

  expect(spy).toHaveBeenCalled();
});
