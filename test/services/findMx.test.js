const { expect } = require('@jest/globals');
const findMx = require('../../src/services/findMx');

test('find mxrecord for given host if exists', async () => {
    let domain = 'walmart.com'
    const addresses = await findMx(domain);
    expect(addresses).toHaveLength(2);
})

test('find mxrecord for given host if exists', async () => {
    let domain = 'http://x.com'
    try {
        const addresses = await findMx(domain);
    } catch (e){
        expect(e.message).toEqual(expect.stringContaining("queryMx ENOTFOUND"))
    }
})