const { expect } = require("@jest/globals");
const { generateDomains } = require("../../src/services/generateDomains");

test("read domain from default text file", async () => {
  const addresses = await generateDomains();
  expect(addresses).toHaveLength(1000);
});
