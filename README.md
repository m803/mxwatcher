# mxwatcher

Mxwatcher backend cron job to crawl Fortune Top 1000 companies' MX Records to find their email services

Runs on NodeJS backend using MongoDB Atlas

Linked to a MongoDB Charts frontend at [here](https://charts.mongodb.com/charts-mxwatcher-qyfzt/public/dashboards/610953ca-3e50-42d1-8137-ff4ae3dbb37b)


