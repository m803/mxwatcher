const dns = require("dns");


const findMx = async (domain) => {
    return new Promise((resolve, reject) =>{
        dns.resolveMx(domain, (err, address) => {
          if (err) {
            reject(err);
          } else {
            resolve(address);
          }
        });
    })
  };
  
  
  module.exports = findMx