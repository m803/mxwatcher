const fs = require("fs");
const csv = require("csv-parser");
const path = require("path");

const fileName = "/fortune_1000_domains.txt";
const resourcePath = path.join(__dirname, "../resources");
const defaultFilePath = resourcePath + fileName;

const generateDomains = async (filePath = defaultFilePath) => {
  const domains = [];
  const parser = fs
    .createReadStream(filePath)
    .pipe(
      csv({ headers: ["Company", "SubIndustry", "Industry", "Host", "Row"] })
    );
  for await (const row of parser) {
    const domain = row.Host.split(/(https?:\/\/)?(www.)?(.*)?/)[3];
    domains.push({ company: row.Company, domain });
  }
  return domains;
};

const generateDBRecords = async (filePath = defaultFilePath) => {
  const domains = [];
  const parser = fs
    .createReadStream(filePath)
    .pipe(
      csv({ headers: ["Company", "SubIndustry", "Industry", "Host", "Row"] })
    );
  for await (const row of parser) {
    const domain = row.Host.split(/(https?:\/\/)?(www.)?(.*)?/)[3];
    domains.push({
      company: row.Company,
      subindustry: row.SubIndustry,
      industry: row.Industry,
    });
  }
  return domains;
};

module.exports = { generateDomains };
