const findMx = require("./findMx");
const { generateDomains } = require("./generateDomains");
const emailServiceRepo = require("../database/emailServiceRepo");

var fs = require("fs");
//TODO get mxrecords for companies with different email addresses from domain

const crawlForEmailService = async () => {
  const exchanges = [];
  const domains = await generateDomains();
  const today = new Date();
  today.setUTCHours(0, 0, 0, 0);
  for (const { company, domain } of domains) {
    try {
      const mxRecord = await findMx(domain);
      const service = getServiceFromRecord(mxRecord);
      exchanges.push({ company, service, date: today });
    } catch (e) {
      exchanges.push({ company, service: null, date: today });
    }
  }
  try {
    await emailServiceRepo.saveMany(exchanges);
  } catch (e) {
    console.log(e);
  }
};

const hasAlreadyCrawled = async () => {
  let today = new Date();
  today.setUTCHours(0, 0, 0, 0);
  const res = await emailServiceRepo.findAllBy({
    date: today,
  });
  return (await res.count()) == 1000 ? true : false;
};

const getServiceFromRecord = (record) => {
  const lowestPriorityFirst = (a, b) =>
    a.priority - b.priority || a.exchange.localeCompare(b.exchange);
  record.sort(lowestPriorityFirst);
  const exchangeArr = record[0].exchange.split(".");
  const service = exchangeArr[exchangeArr.length - 2];
  return service;
};

module.exports = { crawlForEmailService, hasAlreadyCrawled };
