const dbo = require("./mongoConn");

const save = async (company, service, date) => {
  let db_connect = dbo.getDb();
  const res = await db_connect.collection("EmailService").insertOne({
    company,
    service,
    date,
  });
};

const saveMany = async (emailServiceList) => {
  let db_connect = dbo.getDb();
  const res = await db_connect
    .collection("EmailService")
    .insertMany(emailServiceList);
  return res;
};

const findByCompany = async (company) => {
  let db_connect = dbo.getDb();
  const res = await db_connect.collection("EmailService").findOne({ company });
  return res;
};

const findAllBy = async (obj) => {
  let db_connect = dbo.getDb();
  const res = await db_connect.collection("EmailService").find(obj);
  return res;
};

module.exports = { findByCompany, save, saveMany, findAllBy };
