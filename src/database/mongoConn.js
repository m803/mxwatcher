const { MongoClient } = require("mongodb");
const password = process.env.MONGO_PASSWORD
const uri = `mongodb+srv://RWuser:${password}@cluster0.fsssz.mongodb.net/MxWatcher?retryWrites=true&w=majority`;
const client = new MongoClient(uri, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

var _db;

module.exports = {
  connectToServer: async () => {
    try {
      let db = await client.connect();
      _db = db.db("MxWatcher");
      console.log("Successfully connected to MongoDB.");
    } catch (e) {
      console.log(e);
    }
  },

  getDb: () => {
    return _db;
  },
};
