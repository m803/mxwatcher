const dbo = require("./mongoConn");

const saveMany = async (emailServiceList) => {
  let db_connect = dbo.getDb();
  const res = await db_connect
    .collection("CompanyDetails")
    .insertMany(emailServiceList);
  return res;
};

module.exports = { findByCompany, save, saveMany };
