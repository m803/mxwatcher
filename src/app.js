const express = require("express");
const cron = require("node-cron");

if (process.env.NODE_ENV !== "production") {
  require("dotenv").config();
}

const dbo = require("./database/mongoConn");
const {
  crawlForEmailService,
  hasAlreadyCrawled,
} = require("./services/crawlForEmailService");

const app = express();

app.set("PORT", process.env.PORT || 3000);

app.get("/", (req, res) => {
  res.status(200).send("App running");
});

app.listen(app.get("PORT"), () =>
  console.log(`Server running on port ${app.get("PORT")}`)
);

cron.schedule("0 */2 * * *", async () => {
  console.log("------------");
  console.log("Running Cron Job");

  try {
    await dbo.connectToServer();
    if (await hasAlreadyCrawled()) {
      console.log("Already crawled for " + new Date().toLocaleString());
    } else {
      console.log("Crawling entries for " + new Date().toLocaleString());
      await crawlForEmailService();
      console.log("Finished saving entries for " + new Date().toLocaleString());
    }
    console.log("Finished Cron Job");
    console.log("------------");
  } catch (e) {
    console.log(
      "Unable to run email cronjob at " + new Date().toLocaleString()
    );
    console.error(e);
  }
});

